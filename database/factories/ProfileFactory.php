<?php

namespace Database\Factories;

use App\Models\Profile;
use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProfileFactory extends Factory
{
    protected $model = Profile::class;

    public function definition()
    {
        return [
            'uid' => $this->faker->numberBetween(1, 9999999999),
            'country_id' => Country::inRandomOrder()->first()->id,
            'name' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->unique()->safeEmail,
            'card_no' => $this->faker->creditCardNumber,
            'card_cvv' => $this->faker->numberBetween(100, 999),
            'comment' => $this->faker->sentence,
        ];
    }
}
