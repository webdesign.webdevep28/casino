<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name' => 'WebDevep',
            'email' => 'webdesign.webdevep28@gmail.com',
            'password' => Hash::make('J2vSod7ebguhqDI6c5Wc9EmUK5Qf'),
        ]);
    }
}
