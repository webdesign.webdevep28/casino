<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountrySeeder extends Seeder
{
    public function run()
    {
        $countries = ['Россия', 'Турция', 'Канада', 'Австралия', 'Новая Зеландия', 'Индия', 'Литва', 'Латвия', 'Польша', 'Иная страна'];

        foreach ($countries as $country) {
            Country::create(['title' => $country]);
        }
    }
}
