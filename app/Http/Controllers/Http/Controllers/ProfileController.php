<?php

namespace App\Http\Controllers\Http\Controllers;

ini_set('display_errors', 'On');
error_reporting(E_ALL);

use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $query = Profile::with('country')->orderBy('id', 'desc');

        if ($request->has('country_id') && !empty($request->country_id)) {
            $query->where('country_id', $request->country_id);
        }

        if ($request->has('email') && !empty($request->email)) {
            $query->where('email', 'like', '%' . $request->email . '%');
        }

        $profiles = $query->paginate(10);

        return response()->json($profiles);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'uid' => 'required|string|max:255',
            'country_id' => 'required|integer|exists:countries,id',
            'email' => 'required|string|email|max:255|unique:profiles',
            'name' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:255',
            'card_no' => 'nullable|string|max:255',
            'card_cvv' => 'nullable|string|max:255',
            'comment' => 'nullable|string|max:1000'
        ]);

        $profile = Profile::create($validatedData);

        return response()->json($profile, 201);
    }

    public function destroy($id)
    {
        $profile = Profile::findOrFail($id);
        $profile->delete();

        return response()->json(null, 204);
    }

}
