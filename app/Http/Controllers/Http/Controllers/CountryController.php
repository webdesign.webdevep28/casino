<?php

namespace App\Http\Controllers\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {
        $countries = Country::select('id', 'title')->get(); // Выбираем только id и title

        return response()->json($countries);
    }
}
