<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $fillable = [
        'uid',
        'country_id',
        'email',
        'name',
        'phone',
        'card_no',
        'card_cvv',
        'comment'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}

