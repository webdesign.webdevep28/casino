<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    protected static string $fileName = 'storage/debug.log';
    protected static mixed $f;

    public function register(): void
    {
    }

    public function boot(): void
    {
//        self::$fileName = __DIR__ . '/../../storage/logs/debug.log';
//        self::$fileName = __DIR__ . '/debug.log';
    }

    /**
     * @param mixed $message
     * @param string $messageType : alert, critical, debug, emergency, error, info, log, notice, warning
     * @return void
     */
    public static function put($message, string $messageType, array $trace): void
    {
        if (is_array($message) || is_object($message)) $message = json_encode($message);

        $root_path = str_replace(DIRECTORY_SEPARATOR . 'public', '', $_SERVER['DOCUMENT_ROOT']);

        if (!$trace || empty($trace)) {
            $trace = debug_backtrace();
        }

        $data = array(
            'file' => str_replace($root_path, '', $trace[0]['file']),
            'line' => $trace[0]['line']
        );

        $output = date('H:i') . " ➤ {$data['file']} [" . strtoupper($messageType) . "] ({$data['line']}): {$message}\r\n";

        self::$f = fopen($_SERVER['DOCUMENT_ROOT'] . "/" . self::$fileName, 'a+');

        if (flock(self::$f, LOCK_EX)) { // Попытка захватить эксклюзивную блокировку
            fwrite(self::$f, $output);
            fflush(self::$f); // Очищает выводные буферы системы записи PHP
            flock(self::$f, LOCK_UN); // Снятие блокировки
        } else {
            echo "Не удалось получить блокировку для записи.";
        }

        fclose(self::$f);
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function alert($message): void
    {
        static::put($message, 'alert', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function critical($message): void
    {
        static::put($message, 'critical', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function debug($message): void
    {
        static::put($message, 'debug', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function emergency($message): void
    {
        static::put($message, 'emergency', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function error($message): void
    {
        static::put($message, 'error', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function info($message): void
    {
        static::put($message, 'info', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function notice($message): void
    {
        static::put($message, 'notice', debug_backtrace());
    }

    /**
     * @param mixed $message
     * @return void
     */
    public static function warning($message): void
    {
        static::put($message, 'warning', debug_backtrace());
    }
}
