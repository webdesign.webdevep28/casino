<?php

use App\Http\Controllers\Http\Controllers\ProfileController;
use App\Http\Controllers\Http\Controllers\CountryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::apiResource('profiles', ProfileController::class);
Route::get('/countries', [CountryController::class, 'index']);
